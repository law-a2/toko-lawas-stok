package cache

import (
	"encoding/json"
	"time"

	"github.com/go-redis/redis/v7"
	"gitlab.com/law-a2/toko-lawas-stok/models"
)

type redisCache struct {
	host    string
	db      int
	expires time.Duration
}

func NewRedisCache(host string, db int, exp time.Duration) ProductCache {
	return &redisCache{
		host:    host,
		db:      db,
		expires: exp,
	}
}	

func (cache *redisCache) getClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     cache.host,
		Password: "",
		DB:       cache.db,
	})
}

func (cache *redisCache) Set(key string, product *models.Product) {
	client := cache.getClient()

	product_json, err := json.Marshal(product)

	if err != nil {
		panic(err)
	}

	client.Set(key, product_json, cache.expires*time.Second)
}

func (cache *redisCache) Get(key string) *models.Product {
	client := cache.getClient()

	val, err := client.Get(key).Result()

	if err != nil {
		return nil
	}

	product := models.Product{}
	err = json.Unmarshal([]byte(val), &product)
	if err != nil {
		panic(err)
	}

	return &product
}

func (cache *redisCache) Ping() (string, error) {
	client := cache.getClient()
	return client.Ping().Result()
}

func (cache *redisCache) Invalidate(key string) {
	client := cache.getClient()

	client.Del(key)
}