package cache

import (
	"gitlab.com/law-a2/toko-lawas-stok/models"
)

type ProductCache interface {
	Set(key string, value *models.Product)
	Get(key string) *models.Product
	Invalidate(key string)
	Ping() (string, error)
}