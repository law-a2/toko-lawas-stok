import requests

product_data = [
     {
        "ID": "1",
        "Name": "White shoe",
        "Picture": "https://static.nike.com/a/images/c_limit,w_318,f_auto/t_product_v1/91f481e7-5887-454a-8d04-c485c70052fe/air-force-1-pltaform-shoes-pwWPHX.png",
        "Price": 100000,
        "Quantity": 100
    },
    {
        "ID": "2",
        "Name": "Black shoe",
        "Picture": "https://static.nike.com/a/images/t_default/28862ef5-d0f9-488c-9d37-702cd834ea2f/air-force-1-07-shoe-J29nBv.png",
        "Price": 120000,
        "Quantity": 100
    },
    {
        "ID": "3",
        "Name": "Blue shoe",
        "Picture": "https://s3.bukalapak.com/img/3699602539/s-463-463/data.png.webp",
        "Price": 150000,
        "Quantity": 100
    },
    {
        "ID": "4",
        "Name": "Black shoe",
        "Picture": "https://cf.shopee.co.id/file/e4b79dc56ca5eec41bd18f7110556ff9",
        "Price": 200000,
        "Quantity": 100
    },
    {
        "ID": "5",
        "Name": "Black shoe",
        "Picture": "https://cf.shopee.co.id/file/e4b79dc56ca5eec41bd18f7110556ff9",
        "Price": 300000,
        "Quantity": 100
    },
    {
        "ID": "6",
        "Name": "Green shoe",
        "Picture": "https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/7b5a7649-e450-47b2-ab50-d01be9bf121e/air-presto-id-shoe.png",
        "Price": 400000,
        "Quantity": 100
    }
]

for product in product_data:
    requests.post("http://35.223.24.101:4000/product", json = product)