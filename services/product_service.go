package services

import (
	"gitlab.com/law-a2/toko-lawas-stok/cache"
	"gitlab.com/law-a2/toko-lawas-stok/dto"
	"gitlab.com/law-a2/toko-lawas-stok/models"
	"gitlab.com/law-a2/toko-lawas-stok/repositories"
)

var(
	productCache= cache.NewRedisCache("redis:6379", 1, 600)
)

type ProductService interface {
	GetAllProduct() ([]models.Product, error)
	CreateProduct(dto.ProductRequest) (models.Product, error)
	GetProduct(id string) (models.Product, error)
	ReduceProduct(id string) (models.Product, error)
	UpdateProduct(dto.ProductRequest, string) (models.Product, error)
	DeleteProduct(id string) error
}

type productService struct {
	productRepository repositories.ProductRepository
}

func NewProductService(p repositories.ProductRepository) ProductService {
	return &productService{
		productRepository: p,
	}
}

func (p productService) GetAllProduct() ([]models.Product, error) {
	return p.productRepository.GetAll()
}

func (p productService) CreateProduct(request dto.ProductRequest) (models.Product, error) {
	product := models.Product {
		ID: request.ID,
		Name: request.Name,
		Picture: request.Picture,
		Price: request.Price,
		Quantity: request.Quantity,
	}
	
	productCache.Set(product.ID, &product)
	return p.productRepository.Create(product)
}

func (p productService) GetProduct(id string) (models.Product, error) {
	var oldProduct *models.Product  = productCache.Get(id)

	if(oldProduct == nil){
		oldProduct, err := p.productRepository.FindById(id)	
		productCache.Set(id, &oldProduct)
		return oldProduct, err
	} else {
		return *oldProduct, nil
	}
	
}

func (p productService) ReduceProduct(id string) (models.Product, error) {
	product, err := p.GetProduct(id)

	if err != nil {
		return models.Product{}, err
	}

	product.Quantity = product.Quantity - 1

	productCache.Invalidate(id)
	return p.productRepository.Update(product)
}
 
func (p productService) UpdateProduct(request dto.ProductRequest, id string) (models.Product, error) {
	product, err := p.GetProduct(id)

	if err != nil {
		return models.Product{}, err
	}

	product.Name = request.Name
	product.Picture = request.Picture
	product.Price = request.Price
	product.Quantity = request.Quantity

	productCache.Invalidate(id)
	return p.productRepository.Update(product)
}

func (p productService) DeleteProduct(id string) error {
	productCache.Invalidate(id)
	return p.productRepository.Delete(id)
}

