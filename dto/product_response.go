package dto

type ProductResponse struct {
	ID       string `json:"ID"`
	Name     string `json:"Name"`
	Picture  string `json:"Picture"`
	Price    int64  `json:"Price"`
	Quantity int64  `json:"Quantity"`
}