package router 

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/law-a2/toko-lawas-stok/controllers"
	"gitlab.com/law-a2/toko-lawas-stok/database"
	"gitlab.com/law-a2/toko-lawas-stok/repositories"
	"gitlab.com/law-a2/toko-lawas-stok/services"
)

func SetupRouter(router *gin.Engine) {

	// database
	db, err := database.GetClient()
	if err != nil {
		log.Println("ERROR | database client error")
		return
	}

	productRepository := repositories.NewProductRepository(db)
	productService := services.NewProductService(productRepository)
	productController := controllers.NewProductController(productService)

	base := router.Group("")
	productController.Setup(base)
}