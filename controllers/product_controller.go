package controllers

import (
	"errors"
	"net/http"
	"gorm.io/gorm"
	"github.com/gin-gonic/gin"
	"gitlab.com/law-a2/toko-lawas-stok/dto"
	"gitlab.com/law-a2/toko-lawas-stok/models"
	"gitlab.com/law-a2/toko-lawas-stok/services"
)

type ProductController interface {
	Setup (router *gin.RouterGroup)
	GetAllProduct(c *gin.Context)
	CreateProduct(c *gin.Context)
	GetProduct(c *gin.Context)
	UpdateProduct(c *gin.Context)
	DeleteProduct(c *gin.Context)
}

type productController struct {
	productService services.ProductService
}

func NewProductController(p services.ProductService) ProductController {
	return &productController{
		productService: p,
	}
}

func (p productController) Setup(router *gin.RouterGroup) {
	productAPI := router.Group("/product")
	productAPI.GET("/:id", p.GetProduct)
	productAPI.GET("", p.GetAllProduct)
	productAPI.POST("", p.CreateProduct)
	productAPI.PUT("/:id", p.UpdateProduct)
	productAPI.DELETE("/:id", p.DeleteProduct)
	productAPI.POST("/reduce/:id", p.ReduceProduct)
}

func (p productController) GetProduct(c* gin.Context) {
	id := c.Param("id")

	res, err := p.productService.GetProduct(id)


	if (errors.Is(err, gorm.ErrRecordNotFound)) {
		c.JSON(http.StatusNotFound, dto.BaseResponse{
			Message: "PRODUCT_NOT_FOUND",
		})
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success get product",
		Data:    convertToResponse(res),
	})
}

func (p productController) GetAllProduct(c *gin.Context) {

	res, err := p.productService.GetAllProduct()

	if (errors.Is(err, gorm.ErrRecordNotFound)) {
		c.JSON(http.StatusNotFound, dto.BaseResponse{
			Message: "PRODUCT_NOT_FOUND",
		})
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success get product",
		Data:    convertToListResponse(res),
	})
}

func (p productController) CreateProduct(c *gin.Context) {
	var product dto.ProductRequest
	err := c.ShouldBindJSON(&product)

	if err != nil {
		c.JSON(http.StatusBadRequest, dto.BaseResponse{
			Message: "INVALID_PRODUCT_REQUEST",
		})
		return
	}

	res, err := p.productService.CreateProduct(product)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success create product",
		Data:    convertToResponse(res),
	})
}

func (p productController) UpdateProduct(c *gin.Context) {
	id := c.Param("id")

	var product dto.ProductRequest
	err := c.ShouldBindJSON(&product)

	if err != nil {
		c.JSON(http.StatusBadRequest, dto.BaseResponse{
			Message: "INVALID_PRODUCT_REQUEST",
		})
		return
	}

	res, err := p.productService.UpdateProduct(product, id)

	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success update question",
		Data:    convertToResponse(res),
	})
}

func (p productController) DeleteProduct(c *gin.Context) {
	id := c.Param("id")

	err := p.productService.DeleteProduct(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success delete question",
	})
}

func (p productController) ReduceProduct(c *gin.Context) {
	id := c.Param("id")

	res, err := p.productService.ReduceProduct(id)

	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success reduce product",
		Data:    convertToResponse(res),
	})
}

func convertToResponse(product models.Product) (dto.ProductResponse) {
	product_response := dto.ProductResponse {
		ID: product.ID,
		Name: product.Name,
		Picture: product.Picture,
		Price: product.Price,
		Quantity: product.Quantity,
	}

	return product_response
}

func convertToListResponse(products []models.Product) ([]dto.ProductResponse) {
	var res []dto.ProductResponse
	for _, j:= range products {
		res = append(res, convertToResponse(j))
	}

	return res
}

