package repositories

import (
	"gitlab.com/law-a2/toko-lawas-stok/models"
	"gorm.io/gorm"
)

type ProductRepository interface {
	GetAll() ([]models.Product, error)
	Create(models.Product) (models.Product, error)
	FindById(id string) (models.Product, error)
	Update(models.Product) (models.Product, error)
	Delete(id string) error
}

type productRepository struct {
	db *gorm.DB
}

func NewProductRepository(db *gorm.DB) ProductRepository {
	return &productRepository{
		db: db,
	}
}

func (p productRepository) GetAll() ([]models.Product, error) {
	var products []models.Product
	res := p.db.Find(&products)
	return products, res.Error
}

func (p productRepository) Create(product models.Product) (models.Product, error) {
	res := p.db.Create(&product)
	return product, res.Error
}

func (p productRepository) FindById(id string) (models.Product, error) {
	var product models.Product
	res := p.db.Where("id = ?", id).First(&product)
	return product, res.Error
}

func (p productRepository) Update(product models.Product) (models.Product, error) {
	res := p.db.Save(&product)
	return product, res.Error
}

func (p productRepository) Delete(id string) error {
	res := p.db.Where("id = ?", id).Delete(&models.Product{})
	return res.Error
}
